# Standard Banner

With Standard Banner, users can create a standard banner block,
a pre-designed block with four potential layouts for site builders.
The user is prompted to add images, text and links when adding a Standard
Banner Block, which are slotted into the accessible and responsive layout.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/standard_banner).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/standard_banner).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Navigate to the block layout configuration page.
2. Add a new block of type "Standard Banner".
3. Customize the content, layout, and visibility settings as desired.


## Maintainers

- Ryan McConnell- [RyanCMcConnell](https://www.drupal.org/u/ryancmcconnell)
