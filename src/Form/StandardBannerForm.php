<?php

namespace Drupal\standard_banner\Form;

use Drupal\block\Entity\Block;
use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\file\FileUsage\FileUsageInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Adds a Standard Banner block in a region and on pages that the user decides.
 */
class StandardBannerForm extends FormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The file usage service.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected $fileUsage;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs StandardBannerForm.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\file\FileUsage\FileUsageInterface $file_usage
   *   The file usage service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger_service
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager service.
   */
  public function __construct(
        ModuleHandlerInterface $module_handler,
        ConfigFactoryInterface $config_factory,
        FileUsageInterface $file_usage,
        MessengerInterface $messenger_service,
        EntityTypeManagerInterface $entity_type_manager
    ) {
    $this->moduleHandler = $module_handler;
    $this->configFactory = $config_factory;
    $this->fileUsage = $file_usage;
    $this->messenger = $messenger_service;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
          $container->get('module_handler'),
          $container->get('config.factory'),
          $container->get('file.usage'),
          $container->get('messenger'),
          $container->get('entity_type.manager')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'standard_block_banner_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $standard_banner_path = $this->moduleHandler->getModule('standard_banner')->getPath();
    $layout_options = [
      1 => '<strong class="sb_layout_option">Layout 1</strong><br><img width="200x" src="/' . $standard_banner_path . '/images/layout1.png" alt="Layout Option 1" />',
      2 => '<strong class="sb_layout_option">Layout 2</strong><br><img width="200x" src="/' . $standard_banner_path . '/images/layout2.png" alt="Layout Option 2" />',
      3 => '<strong class="sb_layout_option">Layout 3</strong><br><img width="200x" src="/' . $standard_banner_path . '/images/layout3.png" alt="Layout Option 3" />',
      4 => '<strong class="sb_layout_option">Layout 4</strong><br><img width="200x" src="/' . $standard_banner_path . '/images/layout4.png" alt="Layout Option 4" />',
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title:'),
      '#required' => TRUE,
    ];

    $form['layout'] = [
      '#type' => 'radios',
      '#title' => $this->t('Layout'),
      '#options' => $layout_options,
      '#required' => TRUE,
      '#default_value' => 1,
    ];

    $form['banner_a'] = [
      '#type' => 'fieldset',
      '#title' => 'Banner A',
      '#open' => TRUE,
    ];

    $form['banner_a']['banner_a_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Banner A Image'),
      '#upload_location' => 'public://standard-banner/',
      '#multiple' => FALSE,
      '#description' => $this->t('Allowed extensions: gif png jpg jpeg'),
      '#upload_validators' => [
        'file_validate_extensions' => ['png jpg jpeg gif'],
      ],
      '#required' => TRUE,
    ];
    $form['banner_a']['banner_a_headline'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Banner A Headline:'),
    ];
    $form['banner_a']['banner_a_caption'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Banner A Caption:'),
    ];
    $form['banner_a']['banner_a_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Banner A Button label:'),
    ];
    $form['banner_a']['banner_a_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Banner A Button link:'),
    ];

    $form['banner_b'] = [
      '#type' => 'fieldset',
      '#title' => 'Banner B',
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="layout"]' => [
      ['value' => 2],
            'or',
      ['value' => 3],
            'or',
      ['value' => 4],
          ],
        ],
      ],
    ];

    $form['banner_b']['banner_b_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Banner B Image'),
      '#upload_location' => 'public://standard-banner/',
      '#multiple' => FALSE,
      '#description' => $this->t('Allowed extensions: gif png jpg jpeg'),
      '#upload_validators' => [
        'file_validate_extensions' => ['png jpg jpeg gif'],
      ],
    ];
    $form['banner_b']['banner_b_headline'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Banner B Headline:'),
    ];
    $form['banner_b']['banner_b_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Banner B Button label:'),
    ];
    $form['banner_b']['banner_b_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Banner B Button link:'),
    ];

    $form['banner_c'] = [
      '#type' => 'fieldset',
      '#title' => 'Banner C',
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="layout"]' => [
      ['value' => 2],
            'or',
      ['value' => 3],
            'or',
      ['value' => 4],
          ],
        ],
      ],
    ];

    $form['banner_c']['banner_c_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Banner C Image'),
      '#upload_location' => 'public://standard-banner/',
      '#multiple' => FALSE,
      '#description' => $this->t('Allowed extensions: gif png jpg jpeg'),
      '#upload_validators' => [
        'file_validate_extensions' => ['png jpg jpeg gif'],
      ],
    ];
    $form['banner_c']['banner_c_headline'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Banner C Headline:'),
    ];
    $form['banner_c']['banner_c_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Banner C Button label:'),
    ];
    $form['banner_c']['banner_c_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Banner C Button link:'),
    ];

    $form['banner_d'] = [
      '#type' => 'fieldset',
      '#title' => 'Banner D',
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="layout"]' => [
      ['value' => 3],
            'or',
      ['value' => 4],
          ],
        ],
      ],
    ];

    $form['banner_d']['banner_d_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Banner D Image'),
      '#upload_location' => 'public://standard-banner/',
      '#multiple' => FALSE,
      '#description' => $this->t('Allowed extensions: gif png jpg jpeg'),
      '#upload_validators' => [
        'file_validate_extensions' => ['png jpg jpeg gif'],
      ],
    ];
    $form['banner_d']['banner_d_headline'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Banner D Headline:'),
    ];
    $form['banner_d']['banner_d_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Banner D Button label:'),
      '#states' => [
        'invisible' => [
          ':input[name="layout"]' => [
      ['value' => 4],
          ],
        ],
      ],
    ];

    $form['banner_d']['banner_d_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Banner D Button link:'),
    ];

    $form['banner_e'] = [
      '#type' => 'fieldset',
      '#title' => 'Banner E',
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="layout"]' => [
      ['value' => 3],
            'or',
      ['value' => 4],
          ],
        ],
      ],
    ];

    $form['banner_e']['banner_e_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Banner E Image'),
      '#upload_location' => 'public://standard-banner/',
      '#multiple' => FALSE,
      '#description' => $this->t('Allowed extensions: gif png jpg jpeg'),
      '#upload_validators' => [
        'file_validate_extensions' => ['png jpg jpeg gif'],
      ],
    ];
    $form['banner_e']['banner_e_headline'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Banner E Headline:'),
    ];
    $form['banner_e']['banner_e_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Banner E Button label:'),
      '#states' => [
        'invisible' => [
          ':input[name="layout"]' => [
      ['value' => 4],
          ],
        ],
      ],
    ];

    $form['banner_e']['banner_e_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Banner E Button link:'),
    ];

    $form['banner_f'] = [
      '#type' => 'fieldset',
      '#title' => 'Banner F',
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="layout"]' => [
      ['value' => 4],
          ],
        ],
      ],
    ];

    $form['banner_f']['banner_f_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Banner F Image'),
      '#upload_location' => 'public://standard-banner/',
      '#multiple' => FALSE,
      '#description' => $this->t('Allowed extensions: gif png jpg jpeg'),
      '#upload_validators' => [
        'file_validate_extensions' => ['png jpg jpeg gif'],
      ],
    ];
    $form['banner_f']['banner_f_headline'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Banner F Headline:'),
    ];
    $form['banner_f']['banner_f_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Banner F Button link:'),
    ];

    $form['banner_g'] = [
      '#type' => 'fieldset',
      '#title' => 'Banner G',
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="layout"]' => [
      ['value' => 4],
          ],
        ],
      ],
    ];

    $form['banner_g']['banner_g_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Banner G Image'),
      '#upload_location' => 'public://standard-banner/',
      '#multiple' => FALSE,
      '#description' => $this->t('Allowed extensions: gif png jpg jpeg'),
      '#upload_validators' => [
        'file_validate_extensions' => ['png jpg jpeg gif'],
      ],
    ];
    $form['banner_g']['banner_g_headline'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Banner G Headline:'),
    ];
    $form['banner_g']['banner_g_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Banner G Button link:'),
    ];

    $current_theme = $this->configFactory->get('system.theme')->get('default');
    $current_region_keys = array_keys(system_region_list($current_theme));
    $current_regions = array_combine($current_region_keys, $current_region_keys);

    $form['region'] = [
      '#type' => 'select',
      '#title' => $this->t('Select region to place block in on current theme'),
      '#options' => $current_regions,
      '#required' => TRUE,
    ];

    $form['visibility'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Specify pages by using their paths. Enter one path per line. The "*" character is a wildcard. An example path is /user/* for every user page. &lt;front&gt; is the front page, and is the default.'),
      '#default_value' => "<front>",
      '#required' => TRUE,
    ];

    $form['#attached']['library'][] = 'standard_banner/block';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $submitted_values = $form_state->cleanValues()->getValues();
    $banner_alts = [];
    $banner_files = [];
    foreach ($submitted_values as $submitted_key => $submitted_value) {
      switch ($submitted_key) {
        case 'banner_a_image':
        case 'banner_b_image':
        case 'banner_c_image':
        case 'banner_d_image':
        case 'banner_e_image':
        case 'banner_f_image':
        case 'banner_g_image':
          if (!empty($submitted_value)) {
            $file_storage = $this->entityTypeManager->getStorage('file');
            $banner_files[$submitted_key] = $file_storage->load($submitted_value[0]);
            $this->fileUsage->add($banner_files[$submitted_key], 'standard_banner', 'file', $submitted_value[0]);
          }
          break;

        case 'banner_a_headline':
        case 'banner_b_headline':
        case 'banner_c_headline':
        case 'banner_d_headline':
        case 'banner_e_headline':
        case 'banner_f_headline':
        case 'banner_g_headline':
          $changed_key = str_replace('headline', 'image', $submitted_key);
          $banner_alts[$changed_key] = $submitted_value;
          break;
      }
    }

    // Take each banner file and if alt text is empty, make it the filename.
    foreach ($banner_files as $alt_key => $banner_file) {
      if (empty($banner_alts[$alt_key])) {
        $banner_alts[$alt_key] = $banner_file->getFilename();
      }
    }

    // Tell block entity manager to create a block of type 'standard_banner'.
    $block_content = BlockContent::create(
          [
            'type' => 'standard_banner',
            'info' => $submitted_values['title'],
            'sb_layout' => $submitted_values['layout'],
            'sb_banner_a_image' => [
              'target_id' => $submitted_values['banner_a_image'][0],
              'alt' => $banner_alts['banner_a_image'],
            ],
            'sb_banner_a_headline' => $submitted_values['banner_a_headline'],
            'sb_banner_a_caption' => $submitted_values['banner_a_caption'],
            'sb_banner_a_url' => [
              'uri' => $submitted_values['banner_a_url'],
              'title' => $submitted_values['banner_a_button_label'],
            ],
            'sb_banner_b_image' => [
              'target_id' => $submitted_values['banner_b_image'][0] ?? NULL,
              'alt' => $banner_alts['banner_b_image'] ?? NULL,
            ],
            'sb_banner_b_headline' => $submitted_values['banner_b_headline'],
            'sb_banner_b_url' => [
              'uri' => $submitted_values['banner_b_url'],
              'title' => $submitted_values['banner_b_button_label'],
            ],
            'sb_banner_c_image' => [
              'target_id' => $submitted_values['banner_c_image'][0] ?? NULL,
              'alt' => $banner_alts['banner_c_image'] ?? NULL,
            ],
            'sb_banner_c_headline' => $submitted_values['banner_c_headline'],
            'sb_banner_c_url' => [
              'uri' => $submitted_values['banner_c_url'],
              'title' => $submitted_values['banner_c_button_label'],
            ],
            'sb_banner_d_image' => [
              'target_id' => $submitted_values['banner_d_image'][0] ?? NULL,
              'alt' => $banner_alts['banner_d_image'] ?? NULL,
            ],
            'sb_banner_d_headline' => $submitted_values['banner_d_headline'],
            'sb_banner_d_url' => [
              'uri' => $submitted_values['banner_d_url'],
              'title' => $submitted_values['banner_d_button_label'],
            ],
            'sb_banner_e_image' => [
              'target_id' => $submitted_values['banner_e_image'][0] ?? NULL,
              'alt' => $banner_alts['banner_e_image'] ?? NULL,
            ],
            'sb_banner_e_headline' => $submitted_values['banner_e_headline'],
            'sb_banner_e_url' => [
              'uri' => $submitted_values['banner_e_url'],
              'title' => $submitted_values['banner_e_button_label'],
            ],
            'sb_banner_f_image' => [
              'target_id' => $submitted_values['banner_f_image'][0] ?? NULL,
              'alt' => $banner_alts['banner_f_image'] ?? NULL,
            ],
            'sb_banner_f_headline' => $submitted_values['banner_f_headline'],
            'sb_banner_f_url' => [
              'uri' => $submitted_values['banner_f_url'],
              'title' => $submitted_values['banner_f_headline'],
            ],
            'sb_banner_g_image' => [
              'target_id' => $submitted_values['banner_g_image'][0] ?? NULL,
              'alt' => $banner_alts['banner_g_image'] ?? NULL,
            ],
            'sb_banner_g_headline' => $submitted_values['banner_g_headline'],
            'sb_banner_g_url' => [
              'uri' => $submitted_values['banner_g_url'],
              'title' => $submitted_values['banner_f_headline'],
            ],
          ]
      );
    $block_content->save();

    $block = Block::create(
          [
            'id' => 'standard_banner_block_' . $block_content->id(),
            'plugin' => 'block_content:' . $block_content->uuid(),
            'region' => $submitted_values['region'],
            'provider' => 'block_content',
            'weight' => -100,
            'theme' => $this->configFactory->get('system.theme')->get('default'),
            'visibility' => [
              'request_path' => [
                'id' => 'request_path' ,
                'pages' => $submitted_values['visibility'],
              ],
            ],
            'settings' => [
              'label' => 'Standard Banner: ' . $submitted_values['title'],
              'label_display' => 0,
            ],
          ]
      );
    $block->save();

    $this->messenger->addMessage($this->t('Thank you for adding a Banner Block!'));
    $form_state->setRedirect('entity.block_content.collection');
  }

}
